# Plugin Spec


## HTTP Endpoints

### /create
Create the user first, responses 200 if it is login ready
* username

### /login
execute after got 200 from `/create`
* username
* tokken/password

### /delete
logouts and deletes the user data
* username

### /start
start the fetching service
* username

### /stop
stop the fetching service
* username

### /status
get status over connection (is user logged in)
* username



Websocket or MessageQueue
